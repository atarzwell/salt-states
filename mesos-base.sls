mesosphere-repo:
  pkgrepo.managed:
    - humanname: mesosphere
    - name: deb http://repos.mesosphere.io/debian jessie main
    - file: /etc/apt/sources.list.d/mesosphere.list
    - keyid: E56151BF
    - keyserver: keyserver.ubuntu.com

mesos:
  pkg.installed:
    - require: 
      - pkgrepo: mesosphere-repo
