/opt/spark:
  file.directory:
    - user: root
    - group: root
    - dir_mode: 755


spark-dir:
  cmd.run:
    - name: 'wget -O /opt/spark/spark-1.5.1-bin-hadoop2.6.tgz http://apache.mirror.gtcomm.net/spark/spark-1.5.1/spark-1.5.1-bin-hadoop2.6.tgz'
    - creates: /opt/spark/spark-1.5.1-bin-hadoop2.6.tgz
    - require:
      - file: /opt/spark

extract-spark:
  cmd.run:
    - name: 'tar -C /opt/spark -zxvf /opt/spark/spark-1.5.1-bin-hadoop2.6.tgz'
    - creates: /opt/spark/spark-1.5.1-bin-hadoop2.6
    - require:
      - cmd: spark-dir

