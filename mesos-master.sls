include:
  - mesos-base
zookeeper-pkg:
  pkg.installed:
    - name: zookeeper
    
zookeeper:
  service.running:
    - enable: True
    - require: 
      - pkg: zookeeper-pkg
    - init_delay: 10

mesos-master:
  service.running:
    - enable: True
    - init_delay: 5
    - require:
      - pkg: mesos
