include:
  - mesos-base

mesos-slave:
  service.running:
    - enable: True
    - init_delay: 5
    - require:
      - pkg: mesos
